package com.example.backendcodingchallenge;

import com.example.backendcodingchallenge.service.GitRepositoryService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BackendcodingchallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendcodingchallengeApplication.class, args);
    }
    @Bean
    CommandLineRunner start(GitRepositoryService gitRepositoryService) {
        return args -> {
            System.out.println(gitRepositoryService.getListofRepositories());
            System.out.println(gitRepositoryService.getNumberOfRepositoriesByLanguage("C++"));
        };
    }

}
