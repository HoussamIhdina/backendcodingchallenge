package com.example.backendcodingchallenge.web;

import com.example.backendcodingchallenge.model.Item;
import com.example.backendcodingchallenge.service.GitRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * this class permit to create a Restful web service by using @RestController
 *
 */
@RestController
public class GitRepositoryRessources {

    @Autowired
    GitRepositoryService gitRepositoryService;

    /**
     * this endpoint return list of languages used in
     *  top 100 trending repositories on Github of the previous month,
     * @return
     */
    @GetMapping(value = "/gitRepositoryList")
    public ResponseEntity<Collection<Item>> getRepositoryList() {
        Collection<Item>  items = this.gitRepositoryService.getListofRepositories();
        if (items.isEmpty()) {
            return new ResponseEntity<Collection<Item>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Collection<Item>>(items, HttpStatus.OK);
    }



    /**
     * Creating an endpoint which it permits to get number of repositories which are using a specific language
     * @param language
     * @return
     */
    @GetMapping(value = "/gitNumberRepository/{language}")
    public ResponseEntity<Double> getNumberRepositoriesBySpecificLanguage(@PathVariable("language") String language) {
        Double count = this.gitRepositoryService.getNumberOfRepositoriesByLanguage(language);
        if (count==null) {
            return new ResponseEntity<Double>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Double>(count, HttpStatus.OK);
    }

    /**
     * this endpoint permit to return list of repositories which are using a specific language
     * @param language
     * @return
     */
    @GetMapping(value = "/gitRepositoryList/{language}")
    public ResponseEntity<Collection<Item>> getRepositoryListBySpecificLanguage(@PathVariable("language") String language) {
        Collection<Item>  items = this.gitRepositoryService.getListOfRepositoriesByLanguage(language);
        if (items.isEmpty()) {
            return new ResponseEntity<Collection<Item>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Collection<Item>>(items, HttpStatus.OK);
    }
}
