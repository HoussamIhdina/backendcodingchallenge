package com.example.backendcodingchallenge.model;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *  this class ITEM made for modeling the data of the api response
 *  where i have used just the necessary attributes
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@ToString @Data @NoArgsConstructor @AllArgsConstructor
public class Item {

    private String name;
    private String full_name;
    private Object language;
    private double forks;
    private double watchers;
    private double stargazers_count;
}
