package com.example.backendcodingchallenge.service;

import com.example.backendcodingchallenge.model.Item;

import java.util.List;

/**
 * This interface contains the needed functions to create a service
 */
public interface GitRepositoryService {

    public List<Item> getListofRepositories();
    public double getNumberOfRepositoriesByLanguage(String language);
    public List<Item> getListOfRepositoriesByLanguage(String language);
}
