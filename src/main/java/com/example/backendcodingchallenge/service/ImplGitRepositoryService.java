package com.example.backendcodingchallenge.service;


import com.example.backendcodingchallenge.model.Item;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * this class implements the interface which is used to create the business logic in a different layer
 * separated from @RestController
 */
@Service
public class ImplGitRepositoryService implements GitRepositoryService {

    RestTemplate restTemplate = new RestTemplate();

    /**
     * getListofRepositories permit to get list of languages used in
     * top 100 trending repositories on Github of the previous month,
     *     with the next attributes:
     *
     * fullname of repository
     * language used in this repository
     * number of stars
     * number of forks
     * number of watchers
     *
     * @return
     */
    @Override
    public List<Item> getListofRepositories() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd");
        Date newDate = new Date(c.getTimeInMillis());
        String preMonthDate = df.format(newDate);
        List<Item> items=new ArrayList<>();
        String json = new RestTemplate().getForObject("https://api.github.com/search/repositories?q=created:>"+preMonthDate+"&sort=stars&order=desc&page=1&per_page=100", String.class);
        JSONObject obj = new JSONObject(json);
        JSONArray jsonWh = obj.getJSONArray("items");
        for (int i = 0; i < jsonWh.length(); i++) {
            JSONObject ob = jsonWh.getJSONObject(i);
            Item item= new Item();
            item.setFull_name(ob.getString("full_name"));
            item.setForks(ob.getDouble("forks"));
            item.setLanguage(ob.get(String.valueOf("language")));
            item.setWatchers(ob.getDouble("watchers"));
            item.setName(ob.getString(("name")));
            item.setStargazers_count(ob.getDouble(("stargazers_count")));
            items.add(item);
        }
        return items ;
    }

    /**
     * getNumberOfRepositoriesByLanguage return number of repositories which are using a specific langauge of development
     * @param language
     * @return
     */
    @Override
    public double getNumberOfRepositoriesByLanguage(String language) {
        double count=0;
        List<Item> itemsWithSameNumber = new ArrayList<>();

        for (Item item : getListofRepositories()) {
            if (item.getLanguage().equals(language)) {
                itemsWithSameNumber.add(item);
                count++;
            }
        }
        return count;
    }

    /**
     * getListOfRepositoriesByLanguage permit to get list of repositories which are using a specific language
     * @param language
     * @return
     */
    @Override
    public List<Item> getListOfRepositoriesByLanguage(String language) {
        List<Item> listRepositories = new ArrayList<>();

        for (Item item : getListofRepositories()) {
            if (item.getLanguage().equals(language)) {
                listRepositories.add(item);
            }
        }
        return listRepositories;
    }


}
