### Gemography Backend Coding Challenge
---
This is the backend coding challenge by gemography with the next requirements:

1. Develop a REST microservice that list the languages used by the 100 trending public repos on GitHub.
2. For every language, you need to calculate the attributes below :
	- The list of repos using the language
	- Number of repos using this language


---

## Framework used :

1. JAVA J2EE Spring boot

---

## EndPoints :

1. **http://localhost:8080/gitRepositoryList** :get list of languages used in top 100 trending repositories on Github of the previous month
2. **http://localhost:8080/gitNumberRepository/{language}** :get number of repositories which are using a specific language
3. **http://localhost:8080/gitRepositoryList/{language}** :return list of repositories which are using a specific language

---
## Project architecture :

1. Model
2. Service
3. Web 
